import json
import socket
import sys
import numpy as np
import random

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.5)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


class bot01(NoobBot):
    def __init__(self, socket, name, key):
        NoobBot.__init__(self, socket, name, key)
        self.totalDistanceTraveled = 0
        self.gameTick = 0
        self.totalDistanceBeforeTick = 0
        self.speed = 1
        self.throttleNumber = 0
        self.distanceToNextTurn = 0
        self.angleTrack = []
        self.totalDistanceTraveledLap = 0
        self.totalDistanceTraveledLapBeforeTick = 0
        self.priorInPieceDistance = 0
        self.totalDistance = 0
        self.priorPiece = 0
        self.priorSpeed = 0
        self.acceleration = 0
        self.switchPiecesIndex = []
        self.okToSwitch = False
        self.priorAngle = 0
        self.priorAngularVelocity = 0
        self.priorAngularAcceleration = 0
        self.angularVelocity = 0
        self.angularAcceleration = 0
        self.maxSpeed = 5
        self.turboMsg = ['Banzai!!!' ,'Eat my dust!']
        self.priorLaneCarPosData = {'endLaneIndex': 0, 'startLaneIndex': 0}
        self.carPosData = 0
        self.priorTotalDistanceTraveled = 0
        self.distanceToNextCorner = 999
        self.distanceToSafeZone = 0
        self.endCornerPieceIndex = 0
        self.dragCoefficient = None
        self.v1 = [0,0]
        self.v2 = []
        self.vStartKnown = False
        self.firstBrake = False
        self.turboAvailable = False
        self.cornerPiecesIndex = []
        self.straightPiecesIndex = []
        self.corner180List = []
        self.v1Known = False
        self.maxCornerSpeed = 5

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1)


    def on_game_init(self, data):
        print("Game initiated.")
        self.throttle(1)
        self.track = data['race']['track']['pieces']
        self.laneInitInfo = data['race']['track']['lanes']
        

        trackPieces = len(self.track)
        

        switchPiecesIndex =  self.find_switch_pieces(self.track)
        self.switchPiecesIndex = switchPiecesIndex
        self.calculate_best_lane_in_switch(switchPiecesIndex)

        self.cornerPiecesIndex = self.find_corner_pieces(self.track)
        self.straightPiecesIndex = self.find_straight_pieces(self.track)

        self.indexForTurbo = self.find_index_for_turbo(self.track)

        self.find_180_corner(self.track)

        self.corner180List = self.find_180_corner_list(self.track)

        self.find_90_corner(self.track)
        self.find_135_corner(self.track)

        for i in xrange(trackPieces):
            self.track[i]['laneLength'] = []
            for j in xrange(len(self.laneInitInfo)):
                if self.track[i].get('length', 0) == 0:
                    self.track[i]['laneLength'].append(2 * abs(self.track[i]['angle'])/360. * np.pi * \
                                                       (self.track[i]['radius'] + self.laneInitInfo[j]['distanceFromCenter']))
                else:
                    self.track[i]['laneLength'].append(self.track[i]['length'])
      
        for i in xrange(trackPieces):
            if i == 0:
                self.track[i]['totalDistanceTraveled'] = 0
            elif i in xrange(3, 13) and i not in [4] or i > 18:
                self.totalDistance += self.track[i-1]['laneLength'][1]
                self.track[i]['totalDistanceTraveled'] = self.totalDistance
            else:
                self.totalDistance += self.track[i-1]['laneLength'][0]
                self.track[i]['totalDistanceTraveled'] = self.totalDistance
        self.totalDistance += self.track[-1]['length']
        


    def calculate_best_lane_in_switch(self, switchPiecesIndex):
        switchPiecesIndexMod = [switchPiecesIndex[-1]] + switchPiecesIndex
        totalDistance = 0
        numberOfLanes = len(self.laneInitInfo)
        for i in xrange(len(switchPiecesIndexMod)-1):
            if self.sum_distance_of_pieces(switchPiecesIndexMod[i], switchPiecesIndexMod[i+1]-1, 0) < \
               self.sum_distance_of_pieces(switchPiecesIndexMod[i], switchPiecesIndexMod[i+1]-1, len(self.laneInitInfo)-1):
                self.track[switchPiecesIndexMod[i]]['bestlane'] = 0
            else:
                self.track[switchPiecesIndexMod[i]]['bestlane'] = 1
        

    def sum_distance_of_pieces(self, start, end, lane):
        totalDistance = 0
        if start > end:
            for i in xrange(start, len(self.track)):
                totalDistance += self.get_piece_length(i, lane)
            for i in xrange(0, end+1):
                totalDistance += self.get_piece_length(i, lane)
            return totalDistance
        else:
            for i in xrange(start, end+1):
                totalDistance += self.get_piece_length(i, lane)
            return totalDistance
            
    def get_piece_length(self, i, lane):
        if self.track[i].get('length', 0) == 0: # corner
            angleInRadian = abs(self.track[i]['angle'])/180. * np.pi
            if self.track[i].get('switch', False):
                if self.isLastPieceDoSwitch() or self.isMakingSwitch():
                    if abs(self.track[i]['angle']) == 22.5 and self.track[i]['radius'] == 200 and self.laneInitInfo[-1]['distanceFromCenter'] - self.laneInitInfo[-2]['distanceFromCenter'] == 20:
                        return 81.0538890729000
                    elif abs(self.track[i]['angle']) == 45. and self.track[i]['radius'] == 100 and self.laneInitInfo[-1]['distanceFromCenter'] - self.laneInitInfo[-2]['distanceFromCenter'] == 20:
                        return 81.0287720064000
                    else:
                        lastStartLane = self.priorLaneCarPosData['startLaneIndex']
                        lastEndLane = self.priorLaneCarPosData['endLaneIndex']
                        
                        r1 = self.track[i]['radius'] + \
                             self.laneInitInfo[lastStartLane]['distanceFromCenter']
                        r2 = self.track[i]['radius'] + \
                             self.laneInitInfo[lastEndLane]['distanceFromCenter']
                        return ((r2*np.cos(angleInRadian) - r1)**2 + \
                                (r2*np.sin(angleInRadian))**2)**.5

                else:
                    return (angleInRadian * (self.track[i]['radius'] + self.laneInitInfo[lane]['distanceFromCenter']))
            else:
                if (self.track[i]['angle'] > 0 and self.laneInitInfo[lane]['distanceFromCenter'] > 0) or\
                   (self.track[i]['angle'] < 0 and self.laneInitInfo[lane]['distanceFromCenter'] < 0):
                    return (angleInRadian * (self.track[i]['radius'] - abs(self.laneInitInfo[lane]['distanceFromCenter'])))
                elif (self.track[i]['angle'] > 0 and self.laneInitInfo[lane]['distanceFromCenter'] < 0) or\
                   (self.track[i]['angle'] < 0 and self.laneInitInfo[lane]['distanceFromCenter'] > 0):
                    return (angleInRadian * (self.track[i]['radius'] + abs(self.laneInitInfo[lane]['distanceFromCenter'])))
                    return (angleInRadian * (self.track[i]['radius'] - abs(self.laneInitInfo[lane]['distanceFromCenter'])))
                else:
                    return angleInRadian * self.track[i]['radius']

        elif self.track[i].get('switch', False): 
            if self.isLastPieceDoSwitch() or self.isMakingSwitch():
                if self.track[i]['length'] == 100 and self.laneInitInfo[-1]['distanceFromCenter'] - self.laneInitInfo[-2]['distanceFromCenter'] == 20:
                    return 102.06
                else:
                    lastStartLane = self.priorLaneCarPosData['startLaneIndex']
                    lastEndLane = self.priorLaneCarPosData['endLaneIndex']
                    horizontalDistance = (abs(self.laneInitInfo[lastStartLane]['distanceFromCenter']) +\
                                          abs(self.laneInitInfo[lastEndLane]['distanceFromCenter']))
                    return (horizontalDistance**2 + self.track[i]['length']**2)**.5

            else:
                return self.track[i]['length']
        else:
            return self.track[i]['length']
    
    def find_switch_pieces(self, track):
        l = []
        for i in xrange(len(track)):
            if track[i].get('switch', False):
                l.append(i)
        return l
                            
    def find_corner_pieces(self, track):
        l = []
        for i in xrange(len(track)):
            if track[i].get('angle', False) and track[i-1].get('length', False):
                l.append(i)
        return np.array(l)

    def find_index_for_turbo(self, track):
        totalLength= 0
        index = 0
        length = 0
        for i in self.straightPiecesIndex:
            j = i
            while track[j].get('length', 0):
                length += track[j]['length']
                if j != len(self.track)-1:
                    j += 1
                else:
                    j = 0
            if length > totalLength:
                index = i
                length = 0
        return index


    def find_straight_pieces(self, track):
        l = []
        for i in xrange(len(track)):
            if track[i].get('length', False) and track[i-1].get('angle', False):
                l.append(i)
        return np.array(l)

    def find_180_corner(self, track):
        for i in xrange(len(self.track)):
            totalDegree = 0
            if self.track[i].get('angle', False) > 0:
                init = i
                while self.track[init].get('angle', False) > 0:
                    totalDegree += self.track[init]['angle']
                    init += 1
                if totalDegree >= 135:
                    self.track[i]['180'] = True
            elif self.track[i].get('angle', False) < 0:
                init = i
                while self.track[init].get('angle', False) < 0:
                    totalDegree += self.track[init]['angle']
                    init += 1
                if totalDegree <= -135:
                    self.track[i]['-180'] = True

    def find_90_corner(self, track):
        for i in self.cornerPiecesIndex:
            if i not in self.corner180List:
                totalDegree = 0
                if self.track[i].get('angle', False) > 0:
                    init = i
                    while self.track[init].get('angle', False) > 0:
                        totalDegree += self.track[init]['angle']
                        init += 1
                        if totalDegree >= 90:
                            self.track[i]['90'] = True
                elif self.track[i].get('angle', False) < 0:
                    init = i
                    while self.track[init].get('angle', False) < 0:
                        totalDegree += self.track[init]['angle']
                        init += 1
                        if totalDegree <= -90:
                            self.track[i]['-90'] = True


    def find_135_corner(self, track):
        for i in self.cornerPiecesIndex:
            if i not in self.corner180List:
                totalDegree = 0
                if self.track[i].get('angle', False) > 0:
                    init = i
                    while self.track[init].get('angle', False) > 0:
                        totalDegree += self.track[init]['angle']
                        init += 1
                        if totalDegree >= 135:
                            self.track[i]['135'] = True
                elif self.track[i].get('angle', False) < 0:
                    init = i
                    while self.track[init].get('angle', False) < 0:
                        totalDegree += self.track[init]['angle']
                        init += 1
                        if totalDegree <= -135:
                            self.track[i]['-135'] = True

                
    def find_180_corner_list(self, limit):
        l = []
        for i in xrange(len(self.track)):
            if self.track[i].get('180', False):
                l.append(i)
            elif self.track[i].get('-180', False):
                l.append(i)
        return l

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        self.throttleNumber = throttle

    def on_crash(self, data):
        if self.yourCar == data:
            print("Crash in distance: ") + str(self.totalDistanceTraveled), "pieceIndex: " + str(self.pieceIndex)
        else:
            print data['name'] + ' ' + 'crashed.'


    def on_spawn(self, data):
        if self.yourCar == data:
            print("Spawned back to game.")
        else:
            print data['name'] + ' ' + 'spawned.'
        self.throttle(1)
        

    def on_game_tick(self, data):
        self.gameTick = data

    def on_lap_finished(self, data):
        self.lapData = data
        self.totalDistanceTraveledLap = 0
        self.totalDistanceTraveledLapBeforeTick = 0
        self.priorPiece = 0

        print data['car']['name'], ' finished', 'lap: ' + str(data['lapTime']['lap']),\
            'time: ' + str(data['lapTime']['ticks']) + '\n'


    def on_your_car(self, data):
        self.yourCar = data

    def on_turbo_available(self, data):
        self.turboData = data
        self.turboAvailable = True
        print 'Turbo is ready.'

    def on_dnf(self, data):
        print data['car']['name'] , 'dnf because ', data['reason'] + '.' + '\n'

    def on_finish(self, data):
        print data['name'] , 'finish.' + '\n'

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.on_lap_finished,
            'yourCar': self.on_your_car,
            'dnf': self.on_dnf,
            'finish': self.on_finish,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'gameStart': self.on_game_start,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPositions':
                    msg_map[msg_type](data)
                    try:
                        self.on_game_tick(msg['gameTick'])
                    except KeyError:
                        self.gameTick = 0
                else:
                    msg_map[msg_type](data)
            
            else:
                print("Got {0}".format(msg_type))

            line = socket_file.readline()

    def print_info(self, inPieceDistance):
        print "Lap: " + str(self.lap), "PieceIndex: " + str(self.pieceIndex), "Distance: " + '%f' % self.totalDistanceTraveled,\
            "Speed: " + '%f' % self.speed, "Throttle: " +  str(self.throttleNumber), "Acceleration: " '%f' % self.acceleration,\
            "carAngle: " + '%f' % self.angle, "Lane: " + str(self.lane), "inPieceDistance: " + '%.2f' % inPieceDistance,\
            "GameTick: " +  str(self.gameTick)
        print "AngularVelocity: " '%f' % self.angularVelocity,  "AngularAcceleration: " '%.2f' % self.angularAcceleration,\
            "distanceToNextCorner: " "%.2f" % self.distanceToNextCorner, "distanceToSafeZone: " "%.2f" % self.distanceToSafeZone

    def isPieceIndexInc(self):
        if self.pieceIndex > self.priorPiece:
            self.priorPiece = self.pieceIndex
            return True
        else:
            self.priorLaneCarPosData = self.carPosData['piecePosition']['lane']

            return False

    def isLastPieceDoSwitch(self):
        try:
            return self.priorLaneCarPosData['startLaneIndex'] != \
                        self.priorLaneCarPosData['endLaneIndex']
        except KeyError:
            return False

    def isMakingSwitch(self):
        if self.gameTick > 0:
            return self.carPosData['piecePosition']['lane']['startLaneIndex'] != self.carPosData['piecePosition']['lane']['endLaneIndex']
        else:
            return False

    def calculate_distance(self, inPieceDistance):
        if self.isPieceIndexInc():
            self.totalDistanceTraveled = self.totalDistanceTraveled - self.priorInPieceDistance + \
                                         self.get_piece_length(self.pieceIndex -1 , self.lane) + inPieceDistance
            self.priorInPieceDistance = inPieceDistance
        else:
            self.totalDistanceTraveled = self.totalDistanceTraveled - self.priorInPieceDistance + inPieceDistance
            self.priorInPieceDistance = inPieceDistance

    def calculate_speed(self, inPieceDistance):
        self.speed = self.totalDistanceTraveled - self.priorTotalDistanceTraveled
        self.priorTotalDistanceTraveled = self.totalDistanceTraveled

    def calculate_acceleration(self):
        self.acceleration = self.speed - self.priorSpeed
        self.priorSpeed = self.speed

    def calculate_angular_velocity(self):
        self.angularVelocity = self.angle - self.priorAngle
        self.priorAngle = self.angle

    def calculate_angular_acceleration(self):
        self.angularAcceleration = self.angularVelocity - self.priorAngularVelocity
        self.priorAngularVelocity = self.angularVelocity

    def calculate_distance_to_next_corner(self, inPieceDistance):
        nextCornerIndexMinus1 = self.cornerPiecesIndex[np.argmax(self.cornerPiecesIndex > self.pieceIndex)] - 1
        nextStraightIndex = self.straightPiecesIndex - 2
        self.distanceToNextCorner = self.sum_distance_of_pieces(self.pieceIndex, nextCornerIndexMinus1, self.lane) - inPieceDistance
        self.distanceToSafeZone = self.sum_distance_of_pieces(self.pieceIndex,\
                                                              nextStraightIndex[np.argmax(nextStraightIndex > self.pieceIndex)] ,\
                                                              self.lane) - inPieceDistance
    def compute_max_speed(self):
        nextCornerIndex = self.cornerPiecesIndex[np.argmax(self.cornerPiecesIndex > self.pieceIndex)]
        
        if self.track[nextCornerIndex].get('180', False):
            print '180'
            self.maxCornerSpeed = 61./30.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']-\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])

        elif self.track[nextCornerIndex].get('-180', False):
            print '-180'
            self.maxCornerSpeed = 61./30.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])

        elif self.track[nextCornerIndex].get('90', False):
            print '90', self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter']
            self.maxCornerSpeed = 67./30.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']-\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])

        elif self.track[nextCornerIndex].get('-90', False):
            print '-90',self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter']
            self.maxCornerSpeed = 67./30.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])


        elif self.track[nextCornerIndex].get('135', False):
            print '135'
            self.maxCornerSpeed = 67./30.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']-\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])

        elif self.track[nextCornerIndex].get('-135', False):
            print '-135'
            self.maxCornerSpeed = 67./30.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])


        elif self.track[nextCornerIndex].get('angle', 0) > 0:
            print '<90', self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter']
            self.maxCornerSpeed = 23./10.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']-\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])

        elif self.track[nextCornerIndex].get('angle', False) < 0:
            print '<90', self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter']
            self.maxCornerSpeed = 23./10.*np.sqrt(1./10.)*np.sqrt(self.track[nextCornerIndex]['radius']+\
                                                           self.laneInitInfo[self.carPosData['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter'])




    def switch_lane(self, data):
        self.msg("switchLane", data)
        self.okToSwitch = False

    def move_right_or_left(self, nextPieceIndex):
        if nextPieceIndex in self.switchPiecesIndex and self.okToSwitch:
            if self.track[nextPieceIndex]['bestlane'] == 1:
                self.switch_lane('Right')
            elif self.track[nextPieceIndex]['bestlane'] == 0:
                self.switch_lane('Left')
        elif self.pieceIndex not in self.switchPiecesIndex:
            self.okToSwitch = True

    def use_turbo(self):
        self.msg("turbo", random.choice(self.turboMsg))

    def fullThrottle(self):
        self.throttle(1)

    def fullBreak(self):
        self.throttle(0)

    def driveMax(self, limit):
        if self.speed < limit:
            if abs(self.speed - limit) < .05:
                self.throttle(limit/10.)
            else:
                self.throttle(1)
        elif self.speed > limit:
            self.throttle(0)
        
    def on_car_positions(self, data):
        for i in data:
            if i['id'] == self.yourCar:
                self.carPosData = i

        self.pieceIndex = self.carPosData['piecePosition']['pieceIndex']
        self.lap = self.carPosData['piecePosition']['lap']
        inPieceDistance = self.carPosData['piecePosition']['inPieceDistance']
        self.lane = self.carPosData['piecePosition']['lane']['startLaneIndex']
        self.angle = self.carPosData['angle']
        nextPieceIndex = self.pieceIndex + 1
        self.calculate_distance(inPieceDistance)
        self.calculate_speed(inPieceDistance)
        self.calculate_acceleration()
        self.calculate_angular_velocity()
        self.calculate_angular_acceleration()
        self.calculate_distance_to_next_corner(inPieceDistance)
        if True:
            self.print_info(inPieceDistance)
            print

        if self.turboAvailable and self.pieceIndex == self.indexForTurbo+1:
            self.use_turbo()
            self.turboAvailable = False

        if self.vStartKnown == False:
            if self.v1Known == False:
                if self.gameTick == 0 and self.speed != 0 and self.speed < 10:
                    self.v1 = [self.speed, self.gameTick]
                    self.v1Known = True
                elif self.gameTick == 1:
                    self.v1 = [self.speed, self.gameTick]
                    self.v1Known = True

            if self.gameTick == self.v1[1]+1:
                self.v2 = [self.speed, self.gameTick]
            if self.gameTick == self.v1[1]+2:
                self.v3 = [self.speed, self.gameTick]
                self.dragCoefficient = np.log((self.v2[0]-self.v1[0])/self.v1[0]) + 1
                self.firstBrake = True
                self.vStartKnown = True
                print 'v1: ', self.v1, self.v2, self.v3, self.dragCoefficient
            

        self.compute_max_speed()
        self.move_right_or_left(nextPieceIndex)

        try:
            if self.firstBrake:
                print 'maxCornerSpeed: ', self.maxCornerSpeed, self.distanceToNextCorner/self.speed, np.log(self.maxSpeed/self.speed)/(self.dragCoefficient - 1)
                print

            if self.firstBrake and self.distanceToNextCorner/self.speed < np.log(self.maxCornerSpeed/self.speed)/(self.dragCoefficient - 1):
                self.maxSpeed = self.maxCornerSpeed
                self.driveMax(self.maxSpeed)
            elif self.track[self.pieceIndex].get('angle', False):
                if self.pieceIndex in (self.straightPiecesIndex-1) and abs(self.angle) < 45:
                    self.throttle(1)
                else:
                    self.driveMax(self.maxSpeed)
            elif self.angle < 45:
                self.throttle(1)
            else:
                self.driveMax(self.maxSpeed)
        except ZeroDivisionError:
            self.throttle(1)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = bot01(s, name, key)
        bot.run()
